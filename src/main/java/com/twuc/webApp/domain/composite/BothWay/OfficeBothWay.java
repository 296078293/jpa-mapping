package com.twuc.webApp.domain.composite.BothWay;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class OfficeBothWay {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @OneToMany(cascade = CascadeType.PERSIST,orphanRemoval = true,mappedBy = "officeBothWay")
    private List<StaffBothWay> staffs = new ArrayList();

    public List<StaffBothWay> getList() {
        return staffs;
    }

    public OfficeBothWay(String name) {
        this.name = name;
    }

    public OfficeBothWay() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void add(StaffBothWay staffBothWay){
        if (staffs.contains(staffBothWay)){
            return;
        }
        staffs.add(staffBothWay);
        staffBothWay.setOfficeBothWay(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OfficeBothWay that = (OfficeBothWay) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
