package com.twuc.webApp.domain.composite.BothWay;

import javax.persistence.*;

@Entity
public class StaffBothWay {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @ManyToOne
    private OfficeBothWay officeBothWay;

    public StaffBothWay() {
    }

    public StaffBothWay(String name, OfficeBothWay office) {
        this.name = name;
        this.officeBothWay = office;
    }


    public StaffBothWay(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }

    public void setOfficeBothWay(OfficeBothWay officeBothWay) {
        if (this.officeBothWay != null && this.officeBothWay.equals(officeBothWay))
            return;
        this.officeBothWay = officeBothWay;
        this.officeBothWay.add(this);
    }

    public OfficeBothWay getOfficeBothWay() {
        return officeBothWay;
    }
}
