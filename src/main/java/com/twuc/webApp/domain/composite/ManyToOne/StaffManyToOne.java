package com.twuc.webApp.domain.composite.ManyToOne;

import javax.persistence.*;

@Entity
public class StaffManyToOne {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    //关系级连都控制
    @ManyToOne(optional = false)
    private OfficeManyToOne office;

    public StaffManyToOne() {
    }

    public StaffManyToOne(String name, OfficeManyToOne office) {
        this.name = name;
        this.office = office;
    }

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }

    public StaffManyToOne(String name) {
        this.name = name;
    }

    public void setOffice(OfficeManyToOne office) {
        this.office = office;
    }

    public OfficeManyToOne getOffice() {
        return office;
    }
}
