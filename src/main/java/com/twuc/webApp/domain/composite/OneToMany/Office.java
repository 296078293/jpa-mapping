package com.twuc.webApp.domain.composite.OneToMany;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Office {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = false)
    @JoinColumn(name = "officeId", nullable = false, updatable = false)
    private List<Staff> staffs = new ArrayList<>();

    public Office(String name) {
        this.name = name;
    }

    public Office() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setStaffs(List<Staff> staffs) {
        this.staffs = staffs;
    }

    public List<Staff> getStaffs() {
        return staffs;
    }

    public void add(Staff staff) {
        staffs.add(staff);
    }
}
