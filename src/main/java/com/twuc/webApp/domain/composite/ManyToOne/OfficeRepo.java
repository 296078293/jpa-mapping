package com.twuc.webApp.domain.composite.ManyToOne;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OfficeRepo extends JpaRepository<OfficeManyToOne,Long> {
}
