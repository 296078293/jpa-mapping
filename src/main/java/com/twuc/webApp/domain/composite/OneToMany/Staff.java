package com.twuc.webApp.domain.composite.OneToMany;

import javax.persistence.*;

@Entity

public class Staff {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    public Staff() {
    }

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }

    public Staff(String name) {
        this.name = name;
    }
}
