package com.twuc.webApp.domain.composite.ManyToOne;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class OfficeManyToOne {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    public OfficeManyToOne(String name) {
        this.name = name;
    }

    public OfficeManyToOne() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}
