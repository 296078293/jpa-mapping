package com.twuc.webApp.domain.composite.BothWay;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StaffBothWayRepo extends JpaRepository<StaffBothWay,Long> {
}
