package com.twuc.webApp.domain.composite.OneToOne;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //cascade控制级连
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "account")
    private Profile profile;

    @Column
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Account(String name) {
        this.name = name;
    }

    public Account() {
    }

    public Account(Profile profile) {
        this.profile = profile;
    }

    public Long getId() {
        return id;
    }

    public Profile getProfile() {
        return profile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id.equals(account.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void setProfile(Profile profile) {
        if (this.profile != null && this.profile.equals(profile)) {
            return;
        }
        this.profile = profile;
        if (profile != null) {
            this.profile.setAccount(this);
        }
    }
}
