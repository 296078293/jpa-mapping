package com.twuc.webApp;

import com.twuc.webApp.domain.JpaTestBase;
import com.twuc.webApp.domain.composite.ManyToOne.OfficeManyToOne;
import com.twuc.webApp.domain.composite.ManyToOne.OfficeRepo;
import com.twuc.webApp.domain.composite.ManyToOne.StaffManyToOne;
import com.twuc.webApp.domain.composite.ManyToOne.StaffRepo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ManyToOneTest extends JpaTestBase {
    @Autowired
    OfficeRepo officeRepo;
    @Autowired
    StaffRepo staffRepo;

    @Test
    void test_many_to_one() {

        OfficeManyToOne office = new OfficeManyToOne("tw");
        officeRepo.saveAndFlush(office);
        StaffManyToOne staff=new StaffManyToOne("ruifeng",office);
        staffRepo.saveAndFlush(staff);
        Optional<OfficeManyToOne> found = officeRepo.findById(1L);

        assertTrue(found.isPresent());
        assertEquals("tw", found.get().getName());
    }
}
