package com.twuc.webApp;

import com.twuc.webApp.domain.JpaTestBase;
import com.twuc.webApp.domain.composite.OneToOne.Account;
import com.twuc.webApp.domain.composite.OneToOne.AccountRepository;
import com.twuc.webApp.domain.composite.OneToOne.Profile;
import com.twuc.webApp.domain.composite.OneToOne.ProfileRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class OneToOneTest extends JpaTestBase {

    @Autowired
    AccountRepository accountRepository;
    @Autowired
    ProfileRepository profileRepository;
    @Autowired
    EntityManager entityManager;

    @Test
    void should_() {

        Account account = new Account("ruifeng");
        Profile profile = new Profile("springboot jpa");
        account.setProfile(profile);
        profile.setAccount(account);
        accountRepository.saveAndFlush(account);
        entityManager.clear();



        Optional<Account> accountOptional = accountRepository.findById(1L);
        Account account1 = accountOptional.get();
        account1.setProfile(null);




    }
}
