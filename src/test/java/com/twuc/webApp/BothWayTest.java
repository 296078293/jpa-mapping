package com.twuc.webApp;

import com.twuc.webApp.domain.JpaTestBase;
import com.twuc.webApp.domain.composite.BothWay.OfficeBothWay;
import com.twuc.webApp.domain.composite.BothWay.OfficeBothWayRepo;
import com.twuc.webApp.domain.composite.BothWay.StaffBothWay;
import com.twuc.webApp.domain.composite.BothWay.StaffBothWayRepo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BothWayTest extends JpaTestBase {
    @Autowired
    OfficeBothWayRepo officeRepo;
    @Autowired
    StaffBothWayRepo staffRepo;

    @Test
    void test_both_way() {

        OfficeBothWay office_both_way = new OfficeBothWay("office both way");
        StaffBothWay staff_both_way = new StaffBothWay("staff both way");
        officeRepo.save(office_both_way);
        staffRepo.save(staff_both_way);
        staffRepo.flush();

        StaffBothWay staffBothWay = staffRepo.findById(1L).orElseThrow(RuntimeException::new);
        staffBothWay.setOfficeBothWay(office_both_way);

        List<StaffBothWay> list = officeRepo.findById(1L).get().getList();
        assertEquals(1, list.size());
    }
}
