package com.twuc.webApp;

import com.twuc.webApp.domain.JpaTestBase;
import com.twuc.webApp.domain.composite.OneToMany.Office;
import com.twuc.webApp.domain.composite.OneToMany.OfficeRepository;
import com.twuc.webApp.domain.composite.OneToMany.Staff;
import com.twuc.webApp.domain.composite.OneToMany.StaffRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class OneToManyTest extends JpaTestBase {

    @Autowired
    OfficeRepository officeRepository;
    @Autowired
    StaffRepository staffRepository;
    @Autowired
    EntityManager entityManager;

    @Test
    void test_one_to_many() {

        Office office = new Office();
        office.add(new Staff("haha"));
        Office office1 = officeRepository.saveAndFlush(office);
        office1.setStaffs(null);
        entityManager.flush();
        //
        Optional<Staff> foundStaff = staffRepository.findById(1L);
        assertTrue(foundStaff.isPresent());
        assertEquals("haha", foundStaff.get().getName());
    }


}
